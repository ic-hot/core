package ic.network.icotp


import ic.struct.list.List
import ic.struct.list.ext.reduce.find.find


sealed class IcotpMode (

	internal val byteValue : Byte

) {

	object Plain 		: IcotpMode(1)
	object Handshake 	: IcotpMode(2)

	companion object {

		private val icotpModes = List(
			Plain,
			Handshake
		)

		fun byByteValue (byteValue: Byte) = icotpModes.find { it.byteValue == byteValue }

	}

}