@file:Suppress("UNUSED_PARAMETER")


package ic.network.icotp


import ic.lang.IcObject

import ic.serial.IcSerializable


object Ping : IcSerializable {


	override val classNameToDeclare : String get() = Ping::class.java.name

	override fun serializeToIcObject (output: IcObject) {}

	@JvmStatic
	fun parseFromIcObject (icObject: IcObject) = Ping


}