package ic.network.icotp


import ic.network.tcp.TcpConnection
import ic.network.socket.SocketAddress
import ic.lang.ext.casts.asIcObject
import ic.lang.ext.casts.asKotlinObject

import ic.stream.input.ext.readIcObject
import ic.stream.output.ext.writeIcObject


@Throws(Throwable::class)
@Suppress("UNCHECKED_CAST")
fun <Type> sendIcotpRequest (

	socketAddress : SocketAddress,

	mode : IcotpMode = IcotpMode.Plain,

	request : Any?

) : Type {

	val connection = TcpConnection(socketAddress)

	try {

		connection.output.putByte(mode.byteValue)

		when (mode) {

			is IcotpMode.Plain -> {

				connection.output.writeIcObject(request.asIcObject)

				val status = IcotpStatus.byByteValue(connection.input.getNextByteOrThrowEnd())

				when (status) {

					is IcotpStatus.Success -> return connection.input.readIcObject().asKotlinObject as Type

					is IcotpStatus.Error -> throw connection.input.readIcObject().asKotlinObject as Throwable

				}

			}

			is IcotpMode.Handshake -> throw NotImplementedError()

		}

	} finally {

		connection.close()

	}

}