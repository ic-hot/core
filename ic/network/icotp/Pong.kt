@file:Suppress("UNUSED_PARAMETER")


package ic.network.icotp


import ic.lang.IcObject
import ic.serial.IcSerializable


object Pong : IcSerializable {


	override val classNameToDeclare : String get() = Pong::class.java.name

	override fun serializeToIcObject (output: IcObject) {}

	@JvmStatic
	fun parseFromIcObject (icObject: IcObject) = Pong


}