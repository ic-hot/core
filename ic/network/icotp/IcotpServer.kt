package ic.network.icotp


import ic.stream.input.ext.readIcObject
import ic.stream.output.ext.writeIcObject
import ic.base.throwables.*
import ic.network.tcp.TcpConnection
import ic.network.socket.SocketAddress
import ic.network.tcp.TcpServer

import ic.lang.ext.casts.asIcObject
import ic.lang.ext.casts.asKotlinObject


abstract class IcotpServer : TcpServer() {


	@Throws(Throwable::class)
	protected abstract fun handleRequest (socketAddress: SocketAddress, request: Any?) : Any?


	override fun handleConnection (connection: TcpConnection) {

		try {

			val mode = try {
				IcotpMode.byByteValue(connection.input.getNextByteOrThrowEnd())
			} catch (end: End) { return }

			when (mode) {

				is IcotpMode.Plain -> {

					try {

						val request : Any? = connection.input.readIcObject().asKotlinObject

						val response = when (request) {
							is Ping -> Pong
							else -> handleRequest(connection.address, request)
						}

						connection.output.putByte(IcotpStatus.Success.byteValue)

						connection.output.writeIcObject(response.asIcObject)

					} catch (thrown: Throwable) {

						connection.output.putByte(IcotpStatus.Error.byteValue)

						connection.output.writeIcObject(thrown.asIcObject)

						throw thrown

					}

				}

				is IcotpMode.Handshake -> {
					throw NotImplementedError()
				}

			}

		} finally {
			connection.close()
		}

	}


}