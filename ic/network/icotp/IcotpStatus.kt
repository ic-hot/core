package ic.network.icotp


import ic.struct.list.List
import ic.struct.list.ext.reduce.find.find


sealed class IcotpStatus (

	@JvmField internal val byteValue: Byte

) {

	object Success 	: IcotpStatus(1)
	object Error 	: IcotpStatus(2)

	companion object {

		private val icotpStatuses = List<IcotpStatus>(
			Success,
			Error
		)

		@JvmStatic
		fun byByteValue (byteValue: Byte) : IcotpStatus = icotpStatuses.find { it.byteValue == byteValue }

	}

}