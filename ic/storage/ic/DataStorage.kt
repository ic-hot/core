package ic.storage.ic


import ic.storage.Storage
import ic.storage.fs.local.privateFolder
import ic.storage.icsrc.IcSrcStorage


val privateStorage : Storage = IcSrcStorage(
	directory = privateFolder
)