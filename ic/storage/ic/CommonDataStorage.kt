package ic.storage.ic


import ic.storage.Storage
import ic.storage.fs.ext.createFolderIfNotExists

import ic.storage.icsrc.IcSrcStorage


private var commonDataStorageField : Storage? = null

val commonDataStorage : Storage
	@Synchronized get() {
		if (commonDataStorageField == null) {
			commonDataStorageField = IcSrcStorage (
				userIcDirectory
				.createFolderIfNotExists("data")
				.createFolderIfNotExists("common")
			)
		}
		return commonDataStorageField!!
	}
;