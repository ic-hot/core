package ic.storage.ic


import ic.storage.Storage
import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.icsrc.IcSrcStorage
import ic.struct.value.ext.getValue
import ic.struct.value.lazy.Lazy


val commonPublicDirectory : Folder by Lazy {
	publicIcDirectory
	.createFolderIfNotExists("data")
	.createFolderIfNotExists("common")
}


val commonPublicStorage : Storage by Lazy {
	IcSrcStorage(
		directory = commonPublicDirectory
	)
}