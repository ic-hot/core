package ic.storage.ic


import ic.dist.runtimeAppPackageName
import ic.storage.fs.ext.createFolderIfNotExists
import ic.struct.value.ext.getValue
import ic.struct.value.lazy.Lazy


internal val logsDirectory by Lazy {
	userIcDirectory
	.createFolderIfNotExists("logs")
	.createFolderIfNotExists(runtimeAppPackageName)
}