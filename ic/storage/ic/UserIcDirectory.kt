package ic.storage.ic


import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.getUserHomeFolder
import ic.storage.fs.local.userHomeFolder
import ic.struct.value.ext.getValue
import ic.struct.value.lazy.Lazy


internal val userIcDirectory : Folder by Lazy {
	userHomeFolder.createFolderIfNotExists(".ic")
}


fun getUserIcDirectory (
	userName : String
) : Folder {
	return getUserHomeFolder(userName).createFolderIfNotExists(".ic")
}