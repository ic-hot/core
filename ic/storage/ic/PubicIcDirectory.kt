package ic.storage.ic


import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.ext.createIfNotExists
import ic.struct.value.ext.getValue
import ic.struct.value.lazy.Lazy


internal val publicIcDirectory by Lazy {

	try {
		Folder.createIfNotExists("/home/public/.ic/")
	} catch (t: Throwable) {
		userIcDirectory.createFolderIfNotExists("public")
	}

}