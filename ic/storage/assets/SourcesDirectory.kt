package ic.storage.assets


import ic.dist.Distribution
import ic.dist.ext.getPackageSourcesDirectory
import ic.dist.runtimeAppPackageName
import ic.storage.fs.Folder


val sourcesDirectory : Folder = (

	Distribution.runtime.getPackageSourcesDirectory(packageName = runtimeAppPackageName)

)