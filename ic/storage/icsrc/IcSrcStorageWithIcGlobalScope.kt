@file:Suppress("NOTHING_TO_INLINE")


package ic.storage.icsrc


import ic.storage.fs.Folder

import ic.lang.global.initGlobalScope


inline fun IcSrcStorage (

	directory : Folder

) = IcSrcStorage(

	directory = directory,
	initIcScope = { initGlobalScope() }

)