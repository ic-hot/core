package ic.storage.res


import ic.storage.fs.Folder
import ic.storage.fs.ext.getFolderOrSkip
import ic.storage.fs.merge.MergeFolder
import ic.struct.list.ext.lazyReverse

import ic.dist.Distribution
import ic.dist.ext.getPackageSrcDirectoriesOrSkip
import ic.dist.includedPackageNames
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.copyJoin


val resourcesDirectory : Folder = (

	MergeFolder(

		children = (
			includedPackageNames
			.lazyReverse()
			.copyConvert { packageName ->
				Distribution.runtime.getPackageSrcDirectoriesOrSkip(packageName)
				.copyConvert { topSrcDirectory ->
					topSrcDirectory.getFolderOrSkip("res")
				}
			}
			.copyJoin()
		)

	)

)