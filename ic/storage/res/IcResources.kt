package ic.storage.res


import ic.base.strings.ext.replaceAll
import ic.storage.fs.ext.read
import ic.storage.fs.ext.recursive.recursiveGetFileOrNull
import ic.stream.sequence.ext.toString
import ic.util.text.charset.Charset.Companion.Utf8


object IcResources : Resources {

	override fun getStringOrNull (path: String) : String? {
		val actualPath = "text/${ path.replaceAll('.' to '/') }.txt"
		return resourcesDirectory.recursiveGetFileOrNull(actualPath)?.read()?.toString(charset = Utf8)
	}

}