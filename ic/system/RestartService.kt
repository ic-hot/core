package ic.system


import ic.base.throwables.WrongStateException


@Throws(AccessDeniedException::class)
fun restartService (userName: String, serviceName: String, command: String) {

	try {
		stopService(userName, serviceName)
	} catch (wrongState: WrongStateException) {}

	try {
		startBashCommandAsService(
			userName = userName,
			serviceName = serviceName,
			command = command
		)
	} catch (wrongState: WrongStateException) { throw WrongStateException.Error(wrongState) }

}


fun restartService (serviceName: String, command: String) = restartService(runtimeUserName, serviceName, command)