package ic.system


import ic.base.assert.assert
import ic.storage.ic.getUserIcDirectory
import ic.base.throwables.AccessException
import ic.base.throwables.WrongStateException
import ic.parallel.funs.sleep
import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.system.localcmd.LocalBashSession
import ic.util.time.duration.ext.sec

import ic.cmd.bash.bashScriptToCommand
import ic.cmd.bash.nohup
import ic.storage.fs.ext.containsItem


@Throws(AccessException::class, WrongStateException::class)
fun startBashCommandAsService (

	userName : String = runtimeUserName,

	workdir : Folder = ic.storage.fs.local.workdir,

	serviceName : String,

	command : String

) {

	if (userName != "root") {
		if (userName != runtimeUserName) throw AccessException
	}

	val pidsDirectory = getPidsDirectory(userName)

	val logsDirectory = (
		getUserIcDirectory(userName)
		.createFolderIfNotExists("logs")
		.createFolderIfNotExists(serviceName)
	)

	if (pidsDirectory.containsItem(serviceName)) throw WrongStateException

	LocalBashSession(workdir).executeCommand(

		"sudo -u $userName " + bashScriptToCommand(
			nohup(
				bashScriptToCommand(
					"echo $$ > " + pidsDirectory.absolutePath + "/" + serviceName + "; " +
					"while true; do " + bashScriptToCommand(
						command + " &> " + logsDirectory.absolutePath +
						"/sysout-$(date +\"%Y-%m-%d-%H-%M\")"
					) + "; done"
				)
			)
		)

	)

	sleep(1.sec)

	assert { pidsDirectory.containsItem(serviceName) }

}