package ic.system


import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toString
import ic.util.text.charset.Charset
import ic.base.throwables.WrongStateException
import ic.parallel.funs.sleep
import ic.storage.fs.ext.readFileOrNull
import ic.system.funs.executeBashCommand
import ic.util.log.logE
import ic.base.throwables.AccessException
import ic.storage.fs.ext.remove
import ic.util.time.duration.ext.sec


@Throws(AccessException::class, WrongStateException::class)
fun stopService (userName: String, serviceName: String) {

	logE("stopService") { "start" }

	if (userName != "root") {
		if (userName != runtimeUserName) throw AccessException
	}

	val pidsDirectory = getPidsDirectory(userName)

	val pidFile : ByteSequence = pidsDirectory.readFileOrNull(serviceName) ?: throw WrongStateException

	val pidToKill : String = pidFile.toString(Charset.defaultUnix).trim()

	logE("stopService") { "pid: $pidToKill" }

	val response = executeBashCommand(
		"sudo -u $userName kill -TERM -\$(ps -o pgid= $pidToKill | grep -o '[0-9]*')"
	)

	logE("stopService") { "response: $response" }

	// TODO Make mechanism to detect service stopping
	sleep(4.sec)

	pidsDirectory.remove(serviceName)

}


@Throws(WrongStateException::class)
fun stopService (serviceName: String) = stopService(runtimeUserName, serviceName)