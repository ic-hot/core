package ic.system


import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.fs.local.ext.getExisting


internal fun getPidsDirectory (userName: String) : Folder {
	return Folder.getExisting("/tmp").createFolderIfNotExists("service-pids-$userName")
}