package ic.lang.global


import ic.base.reflect.ext.getStaticFieldValue
import ic.base.reflect.getClassByNameOrNull
import ic.base.strings.ext.replaceAll
import ic.base.throwables.NotExistsException
import ic.storage.assets.sourcesDirectory
import ic.storage.fs.ext.read
import ic.storage.fs.ext.recursive.recursiveGetFileOrThrowNotExists
import ic.stream.sequence.ByteSequence
import ic.util.text.charset.Charset
import ic.util.text.charset.ext.bytesToText

import ic.lang.IcObject
import ic.lang.IcSourceFile
import ic.lang.code.program.IcProgram
import ic.lang.code.program.ext.execute
import ic.lang.global.importer.IcImporter


object IcIcImporter : IcImporter() {


	@Throws(NotExistsException::class)
	override fun IcObject.executeNonBaseIcSourceFileOrThrowNotExists (address: String) : IcObject {

		val sourceFileClass : Class<IcSourceFile>? = getClassByNameOrNull(address + "Ic")

		if (sourceFileClass == null) {

			val path = address.replaceAll('.' to '/') + ".ic"

			val byteSequence : ByteSequence = (
				sourcesDirectory.recursiveGetFileOrThrowNotExists(path).read()
			)

			val icSrcText = Charset.defaultUnix.bytesToText(byteSequence)

			val icProgram = IcProgram.parse(icSrcText)

			return icProgram.execute(scope = this)

		} else {

			val sourceFileInstance : IcSourceFile = sourceFileClass.getStaticFieldValue("INSTANCE")

			return sourceFileInstance.execute(scope = this)

		}

	}


}