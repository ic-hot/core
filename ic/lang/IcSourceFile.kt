package ic.lang


interface IcSourceFile {

	fun execute (scope: IcObject) : IcObject

}