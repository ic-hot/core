package ic.app.launch


import ic.base.throwables.NotSupportedException


sealed class LaunchMode {


	object Main : LaunchMode()

	object Test : LaunchMode()


	companion object {

		val runtime : LaunchMode

			get() {

			val launchModeString = System.getenv("IC_LAUNCH_MODE")

			return when (launchModeString) {

				null -> Main

				"main" -> Main

				"test" -> Test

				else -> throw NotSupportedException.Runtime(
					"launchModeString: $launchModeString"
				)

			}

		}

	}


}