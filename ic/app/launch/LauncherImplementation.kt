package ic.app.launch


import ic.app.appArgs
import ic.base.annotations.UsedByReflect
import ic.base.throwables.NotExistsException

import ic.dist.Distribution
import ic.dist.ext.getPackage
import ic.dist.runtimeAppPackageName

import ic.lang.ext.args.argsObject
import ic.lang.ext.casts.asIcObject
import ic.lang.ext.importSourceFile
import ic.lang.global.initGlobalScope


@UsedByReflect
object LauncherImplementation : Launcher {


	@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
	override fun launch (ignored: String?) {

		val appPackage = Distribution.runtime.getPackage(runtimeAppPackageName)

		val mainClassName : String = when (LaunchMode.runtime) {
			LaunchMode.Main -> appPackage.mainClassName
			LaunchMode.Test -> appPackage.testClassName
		}!!

		try {

			launchMainClass(mainClassName)

		} catch (_: NotExistsException) {

			val globalScope = initGlobalScope()
			globalScope.argsObject = appArgs.asIcObject
			globalScope.importSourceFile(mainClassName)

		}

	}


}