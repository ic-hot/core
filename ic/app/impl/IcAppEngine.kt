package ic.app.impl


import ic.base.annotations.UsedByReflect
import ic.app.tier.Tier
import ic.storage.prefs.Prefs

import ic.dist.Distribution
import ic.dist.runtimeAppPackageName
import ic.storage.fs.Folder
import ic.storage.fs.ext.createFolderIfNotExists
import ic.storage.ic.publicIcDirectory
import ic.storage.ic.userIcDirectory
import ic.storage.res.IcResources


@UsedByReflect
object IcAppEngine : AbstractIcAppEngine() {


	override fun getDefaultTier() : Tier {
		val distribution = Distribution.runtime
		if (distribution.projectType == null) {
			return Tier.Production
		} else {
			return Tier.Debug
		}
	}


	override fun initResources() = IcResources


	override fun initPrivateFolder(): Folder {
		return (
			userIcDirectory
			.createFolderIfNotExists("data")
			.createFolderIfNotExists(runtimeAppPackageName)
		)
	}

	override fun initCommonFolder(): Folder {
		return (
			userIcDirectory
			.createFolderIfNotExists("data")
			.createFolderIfNotExists("common")
		)
	}

	override fun initCommonPublicFolder(): Folder {
		return (
			publicIcDirectory
			.createFolderIfNotExists("data")
			.createFolderIfNotExists("common")
		)
	}


	override fun createPrefs (prefsName: String) : Prefs {
		throw NotImplementedError()
	}


}