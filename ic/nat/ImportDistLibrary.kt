package ic.nat


import ic.dist.Distribution
import ic.dist.ext.soDirectory

import ic.nat.jna.importSoLibrary
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized


private val lock = Mutex()

private var isAlreadyImported = false


fun importDistLibrary() {

	lock.synchronized {

		if (isAlreadyImported) return

		importSoLibrary(
			libraryDirectory = Distribution.runtime.soDirectory,
			libraryName = distLibraryName
		)

	}

}