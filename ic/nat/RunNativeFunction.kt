package ic.nat


import ic.nat.jna.runNativeFunction


inline fun <reified Result> runNativeFunction (functionName: String, vararg args: Any?) : Result {

	importDistLibrary()

	return runNativeFunction(
		libraryName = distLibraryName,
		functionName = functionName,
		args = args
	)

}