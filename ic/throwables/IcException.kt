package ic.throwables


import ic.lang.IcObject


class IcException (

	val value : IcObject

) : RuntimeException()