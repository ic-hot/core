package ic.dist.ext


import ic.struct.list.List
import ic.struct.list.ext.plus

import ic.dist.Distribution


internal fun Distribution.getFullDependenciesIncludingSelf (packageName: String) : List<String> {

	return getFullDependencies(packageName) + packageName

}