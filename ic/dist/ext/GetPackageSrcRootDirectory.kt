@file:Suppress("NOTHING_TO_INLINE")


package ic.dist.ext


import ic.base.throwables.NotExistsException
import ic.dist.Distribution
import ic.storage.fs.Folder
import ic.storage.fs.ext.getFolderOrThrowNotExists


@Throws(NotExistsException::class)
fun Distribution.getPackageSrcRootDirectoryOrThrowNotExists (packageName: String) : Folder {

	return srcDirectory.getFolderOrThrowNotExists(packageName)

}


inline fun Distribution.getPackageSrcRootDirectory (packageName: String) : Folder {
	try {
		return getPackageSrcRootDirectoryOrThrowNotExists(packageName)
	} catch (t: NotExistsException) {
		throw NotExistsException.Runtime(
			message = "packageName: $packageName"
		)
	}
}