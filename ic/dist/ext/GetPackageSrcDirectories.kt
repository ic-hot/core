package ic.dist.ext


import ic.base.throwables.NotExistsException
import ic.base.throwables.Skip
import ic.dist.Distribution
import ic.dist.Package.Type.*
import ic.dist.impl.multiplatformDirectoriesNames
import ic.storage.fs.Folder
import ic.storage.fs.ext.getFolderOrSkip
import ic.struct.collection.Collection
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.convert.copyConvertToCollection


@Throws(NotExistsException::class)
fun Distribution.getPackageSrcDirectoriesOrThrowNotExists (

	packageName : String

) : Collection<Folder> {

	val srcRootDirectory : Folder = getPackageSrcRootDirectoryOrThrowNotExists(packageName)

	when (getPackage(packageName).type) {

		Default -> {

			return Collection(srcRootDirectory)

		}

		Multiplatform -> {

			return multiplatformDirectoriesNames.copyConvertToCollection { multiplatformDirName ->
				srcRootDirectory.getFolderOrSkip(multiplatformDirName)
			}

		}

	}

}


fun Distribution.getPackageSrcDirectories (packageName: String) : Collection<Folder> {
	try {
		return getPackageSrcDirectoriesOrThrowNotExists(packageName)
	} catch (t: NotExistsException) {
		throw NotExistsException.Runtime("packageName: $packageName")
	}
}


fun Distribution.getPackageSrcDirectoriesOrSkip (packageName: String) : Collection<Folder> {
	try {
		return getPackageSrcDirectoriesOrThrowNotExists(packageName)
	} catch (t: NotExistsException) {
		throw Skip
	}
}