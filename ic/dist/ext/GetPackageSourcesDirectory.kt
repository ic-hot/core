package ic.dist.ext


import ic.struct.list.ext.lazyReverse

import ic.dist.Distribution
import ic.storage.fs.Folder
import ic.storage.fs.merge.MergeFolder
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.copyJoin


fun Distribution.getPackageSourcesDirectory (packageName: String) : Folder {

	return MergeFolder(

		children = (
			getFullDependenciesIncludingSelf(packageName)
			.lazyReverse()
			.copyConvert {
				getPackageSrcDirectories(packageName = it)
			}
			.copyJoin()
		)

	)

}