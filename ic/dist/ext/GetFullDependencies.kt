package ic.dist.ext


import ic.struct.list.List
import ic.struct.list.ext.foreach.breakableForEach
import ic.struct.list.editable.EditableList

import ic.dist.Distribution
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.ext.reduce.find.none


fun Distribution.getFullDependencies (packageName: String) : List<String> {

	val fullDependencies = EditableList<String>()

	fun addToFullDependencies (packageName: String) {
		if (fullDependencies.none { it == packageName }) {
			fullDependencies.add(packageName)
		}
	}

	getPackage(packageName).dependencies.breakableForEach { dependency ->
		getFullDependencies(dependency).breakableForEach {
			addToFullDependencies(it)
		}
		addToFullDependencies(dependency)
	}

	return fullDependencies

}
