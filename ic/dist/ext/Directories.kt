package ic.dist.ext


import ic.storage.fs.ext.createFolderIfNotExists

import ic.dist.Distribution


val Distribution.icDirectory get() = rootDirectory.createFolderIfNotExists(".ic")

val Distribution.srcDirectory get() = icDirectory.createFolderIfNotExists("src")

val Distribution.soDirectory get() = icDirectory.createFolderIfNotExists("so")