package ic.dist.ext


import ic.struct.set.finite.FiniteSet
import ic.base.throwables.NotExistsException
import ic.base.throwables.UnableToParseException
import ic.dist.Distribution
import ic.dist.Package
import ic.storage.fs.ext.getFolderOrThrowNotExists
import ic.storage.icsrc.IcSrcStorage
import ic.struct.collection.Collection


@Throws(NotExistsException::class)
fun Distribution.getPackageOrThrowNotExists (packageName: String) : Package {

	val packageSrcDir = srcDirectory.getFolderOrThrowNotExists(packageName)

	val packageOrNull : Package? = try {
		IcSrcStorage(packageSrcDir)["package"] as Package?
	} catch (unableToParse: UnableToParseException.Runtime) {
		throw UnableToParseException.Runtime(
			message = "packageName: $packageName"
		)
	}

	return packageOrNull ?: Package(
		stores 			= Collection(),
		dependencies 	= FiniteSet(),
		jarDependencies = FiniteSet(),
		transitives     = FiniteSet(),
		mainClassName 			= null,
		testClassName 			= null,
		type 			= Package.Type.Default
	)

}


fun Distribution.getPackage (packageName: String) : Package {
	return try {
		getPackageOrThrowNotExists(packageName)
	} catch (notExists: NotExistsException) {
		throw NotExistsException.Runtime(
			"distribution: ${ rootDirectory.absolutePath }, packageName: $packageName"
		)
	}
}