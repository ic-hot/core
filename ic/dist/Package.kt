package ic.dist


import ic.base.reflect.ext.className
import ic.serial.IcSerializable
import ic.struct.collection.Collection
import ic.struct.collection.convert.ConvertCollection
import ic.struct.set.finite.FiniteSet

import ic.lang.IcObject
import ic.lang.ext.fields.*
import ic.struct.collection.ext.copy.toArray


data class Package (

	val stores: Collection<Store>,
	val dependencies: FiniteSet<String>,
	val jarDependencies: FiniteSet<String>,
	val transitives: FiniteSet<String>,
	val mainClassName: String?,
	val testClassName: String?,
	val type : Type

) : IcSerializable {


	sealed class Type {
		object Default 			: Type()
		object Multiplatform 	: Type()
	}


	class Store internal constructor (
		val name : String,
		val url : String
	)


	// IcSerializable implementation:

	override val classNameToDeclare get() = Package::class.java.className

	override fun serializeToIcObject (output: IcObject) {
		output["stores"] = ConvertCollection(stores) {
			IcObject(
				"name" to it.name,
				"url" to it.url
			)
		}.toArray()
		output["dependencies"] = dependencies.toArray()
		output["jarDependencies"] = jarDependencies.toArray()
		output["transitives"] = transitives.toArray()
		output["main"] = mainClassName
		output["test"] = testClassName
		output["type"] = when (type) {
			Type.Default -> null
			Type.Multiplatform -> "Multiplatform"
		}
	}

	companion object {

		@JvmStatic
		fun parseFromIcObject (icObject: IcObject) : Package {
			return Package(
				stores = icObject.getAsList("stores") { storeIcObject: IcObject ->
					Store(
						name 	= storeIcObject["name"],
						url 	= storeIcObject["url"]
					)
				},
				dependencies 	= icObject.getAsFiniteSet("dependencies"),
				jarDependencies = icObject.getAsFiniteSet("jarDependencies"),
				transitives 	= icObject.getAsFiniteSet("transitives"),
				mainClassName = icObject["main"],
				testClassName = icObject["test"],
				type = run {
					val typeString : String = icObject["type"]
					when (typeString) {
						"Multiplatform" -> Type.Multiplatform
						else -> Type.Default
					}
				}
			)
		}

	}


}
