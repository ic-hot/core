package ic.dist


import ic.base.annotations.UsedByReflect
import ic.base.throwables.NotExistsException
import ic.serial.IcSerializable
import ic.storage.ext.getOrThrowNotExists
import ic.struct.list.List
import ic.struct.list.ext.reduce.find.findOrThrow
import ic.storage.fs.Folder
import ic.storage.fs.local.ext.getExisting
import ic.storage.fs.local.workdir

import ic.storage.icsrc.IcSrcStorage
import ic.lang.IcObject
import ic.lang.ext.fields.get
import ic.lang.ext.fields.set


class Distribution : IcSerializable {


	val rootDirectory : Folder

	val isPortable : Boolean

	val launchPath 	: String

	val toBuildOnUpdate : Boolean


	sealed class ProjectType (val name: String) {

		object Default 	: ProjectType("Default")
		object Gradle 	: ProjectType("Gradle")

		companion object {

			val all = List<ProjectType>(
				Default,
				Gradle
			)

			@Throws(NotExistsException::class)
			fun byNullableNameOrThrow (name: String?) : ProjectType? {
				if (name == null) return null
				return all.findOrThrow { it.name == name }
			}

			fun byNullableName (name: String?) = try {
				byNullableNameOrThrow(name)
			} catch (notExists : NotExistsException) {
				throw NotExistsException.Runtime(
					"name: $name",
					notExists
				)
			}

		}

	}

	val projectType : ProjectType?


	// Constructors:

	constructor(
		rootDirectory 	: Folder,
		isPortable 		: Boolean,
		launchPath 		: String,
		toBuildOnUpdate	: Boolean = false,
		projectType		: ProjectType? = null
	) {
		this.rootDirectory 		= rootDirectory
		this.isPortable 		= isPortable
		this.launchPath 		= launchPath
		this.toBuildOnUpdate 	= toBuildOnUpdate
		this.projectType 		= projectType
		IcSrcStorage(rootDirectory)["distribution"] = this
	}


	// IcSerializable implementation:

	override val classNameToDeclare : String get() = Distribution::class.java.name

	override fun serializeToIcObject (output: IcObject) {
		output["portable"] 		= isPortable
		output["launchPath"] 	= launchPath
		output["buildOnUpdate"] = toBuildOnUpdate
		output["projectType"] 	= projectType?.name
	}

	private constructor (icObject: IcObject, rootDirectory: Folder) {
		this.rootDirectory = rootDirectory
		this.isPortable      = icObject["portable"] ?: true
		this.launchPath      = icObject["launchPath"]
		this.toBuildOnUpdate = icObject["buildOnUpdate"] ?: false
		this.projectType = ProjectType.byNullableName(icObject["projectType"])
	}


	companion object {


		@JvmStatic
		@UsedByReflect
		fun parseFromIcObject (icObject: IcObject, rootDirectory: Folder) = Distribution(icObject, rootDirectory)


		@Throws(NotExistsException::class)
		fun loadOrThrowNotExists (rootDirectory: Folder) : Distribution {
			return IcSrcStorage(rootDirectory).getOrThrowNotExists(
				"distribution",
				arg = rootDirectory
			)
		}


		private var runtimeValue: Distribution? = null

		@JvmStatic
		val runtime : Distribution get() {
			synchronized(this) {
				if (runtimeValue == null) {
					val rootDirectory = run {
						val env = System.getenv("IC_DISTRIBUTION_PATH")
						if (env == null) {
							workdir
						} else {
							Folder.getExisting(env)
						}
					}
					try {
						runtimeValue = loadOrThrowNotExists(rootDirectory)
					} catch (notExists: NotExistsException) {
						throw NotExistsException.Error("Directory ${ rootDirectory.absolutePath } is not a distribution")
					}
				}
				return runtimeValue!!
			}
		}


	}


}
