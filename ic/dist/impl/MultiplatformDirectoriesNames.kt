package ic.dist.impl


import ic.struct.list.List


val multiplatformDirectoriesNames = List(

	"common",
	"nokm",
	"nonandroid",

	"jvm",
	"nokm-jvm",
	"jvm-nonandroid",
	"nokm-jvm-nonandroid",

	"jvm-ll",

	"ic"

)