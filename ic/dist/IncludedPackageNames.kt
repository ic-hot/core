package ic.dist


import ic.dist.ext.getFullDependenciesIncludingSelf


val includedPackageNames = Distribution.runtime.getFullDependenciesIncludingSelf(packageName = runtimeAppPackageName)